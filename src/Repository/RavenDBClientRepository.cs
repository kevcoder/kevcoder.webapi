using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using kevcoder.webapi.Models;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Raven.Client.Documents.Operations;
using Raven.Client.ServerWide;
using Raven.Client.ServerWide.Operations;

namespace kevcoder.webapi.Repository
{
    public class RavenDBClientRepository : IRepository<Client>
    {
        private const string DATABASE_NAME = "kevcoder";

        private string ConnectionString { get; set; }

        Raven.Client.Documents.IDocumentStore _store { get; set; }
        protected Raven.Client.Documents.IDocumentStore Store
        {
            get
            {
                if (_store == null)
                {
                    _store = new Raven.Client.Documents.DocumentStore
                    {
                        Urls = new string[1] { this.ConnectionString }
                        ,
                        Database = DATABASE_NAME
                        ,
                        Conventions = { }
                    };
                    _store.Initialize();

                    try
                    {
                        _store.Maintenance.ForDatabase(DATABASE_NAME).Send(new GetStatisticsOperation());
                    }
                    catch
                    {
                        _store.Maintenance.Server.Send(new CreateDatabaseOperation(new DatabaseRecord(DATABASE_NAME)));
                    }
                }
                return _store;
            }
        }

        public RavenDBClientRepository(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        public (IEnumerable<Client> results, string error) GetEntities(Expression<Func<Client, bool>> qry)
        {
            List<Client> objs = new List<Client>();

            using (var session = Store.OpenSession())
            {
                var foundClients = session
                .Query<Client>()
                .ToList();

                objs.AddRange(foundClients);
            }
            return (results: objs, error: null);
        }

        public Client GetEntity(string id)
        {
            Client obj = null;
            using (var session = Store.OpenSession())
            {
                obj = session.Load<Client>(id);
            }
            return obj;
        }

        public (bool wasSuccess,string error) SaveEntities(params Client[] documents)
        {
            var results = false;
            string error = null;
            try
            {
                using (var session = Store.OpenSession())
                {
                    foreach (var document in documents)
                    {
                        session.Store(document);
                    }
                    session.SaveChanges();
                }
                results = true;
            }
            catch (Exception e)
            {
                results = false;
                error = e.ToString();
            }
            return (wasSuccess: results, error: error);
        }

        public (bool wasSuccess, string error) UpdateEntities(params Client[] documents)
        {
            using (var session = Store.OpenSession())
            {
                foreach (var doc in documents)
                {
                    var found = this.GetEntity(doc.Id);
                    if (found != null)
                    {
                        session.Store(doc, doc.Id);
                        session.SaveChanges();
                    }
                }
            }
            return (wasSuccess: true, error: null);
        }
    }
}