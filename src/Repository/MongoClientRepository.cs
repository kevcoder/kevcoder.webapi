
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using kevcoder.webapi.Models;
using MongoDB.Driver;

namespace kevcoder.webapi.Repository
{
    public class MnogoClientRepository : IRepository<Client>
    {
        private const string DATABASE_NAME = "kevcoder";

        private readonly IMongoCollection<Client> Clients;

        public MnogoClientRepository(string connectionString)
        {
            this.Clients = new MongoClient(connectionString)
            .GetDatabase(DATABASE_NAME)
            .GetCollection<Client>(nameof(Client));
        }

        public (IEnumerable<Client> results, string error) GetEntities(Expression<Func<Client, bool>> qry)
        {
            var results = Clients.Find(qry).ToEnumerable();
            return (results, null);
        }

        public Client GetEntity(string id) => Clients.Find(b => b.Id == id).FirstOrDefault();

        public (bool wasSuccess, string error) SaveEntities(params Client[] documents)
        {
            Clients.InsertMany(documents);
            return (true, null);
        }

        public (bool wasSuccess, string error) UpdateEntities(params Client[] documents)
        {
            foreach (var d in documents)
            {
                Clients.ReplaceOne(c => c.Id == d.Id, d, new ReplaceOptions() { IsUpsert = true });
            }
            return (true, null);
        }
    }
}