using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace kevcoder.webapi.Repository
{
    public interface IRepository<TEntity> where TEntity : Models.IDocument
    {

        TEntity GetEntity(string id);

        (IEnumerable<TEntity> results, string error) GetEntities(Expression<Func<TEntity, bool>> qry);

        (bool wasSuccess, string error) SaveEntities(params TEntity[] documents);

        (bool wasSuccess, string error) UpdateEntities(params TEntity[] documents);

    }
}
