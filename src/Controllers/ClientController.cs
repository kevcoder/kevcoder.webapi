using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using kevcoder.webapi.Models;
using kevcoder.webapi.Repository;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;
//using kevcoder.webapi.Models;

namespace kevcoder.webapi.Controllers
{
    [EnableCors]
    [Route("api/[controller]")]
    public class ClientController : BaseController
    {
        private readonly ILogger<ClientController> logger;

        public ClientController(IConfiguration configuration, IRepository<Client> repository, ILogger<ClientController> logger)
        : base(configuration, repository)
        {
            this.logger = logger;
        }

        // GET api/client
        [HttpGet]
        public ActionResult<IEnumerable<Client>> Gets()
        {
            var user = User.Claims.FirstOrDefault(c => c.Type == "sub")?.Value;
            logger.LogInformation("{user} is inside the /Client/ GET: {claims}", new object[] { user, User.Claims });

            var clients = Repository.GetEntities((c) => true);

            if (clients.results?.Count() > 0)
            {
                return Ok(clients.results);
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(clients.error))
                    return StatusCode(500, clients.error);
                else
                    return this.NoContent();
            }
        }


        // GET api/client/5
        [HttpGet("id")]
        public ActionResult<string> GetById([FromQuery]string id)
        {
            Client client = Repository.GetEntity(id);

            if (client == null)
                return NoContent();
            else
                return Ok(client);
        }

        // POST api/client
        [HttpPost("")]
        public ActionResult<string> Post([FromBody] Client value)
        {
            (bool wasSuccess, string error) result = ValueTuple.Create<bool, string>(false, null);
            try
            {
                if (!ModelState.IsValid)
                {
                    result.error = string.Join("\n", ModelState.Values.Select(mv => string.Join("\n", mv.Errors.Select(me => me.ErrorMessage))));
                    return BadRequest(result.error);
                }
                if (string.IsNullOrWhiteSpace(value.Id))
                    result = Repository.SaveEntities(value);
                else
                {
                    var upateStatement = new Client[1] { value };

                    result = Repository.UpdateEntities(upateStatement);

                }
            }
            catch (Exception e)
            {
                result.wasSuccess = false;
                result.error = e.ToString();
                
                logger.LogError(e, "Error posting to /api/client", null);
            }
            if (result.wasSuccess)
                return Ok();
            else
                return BadRequest(result.error);
        }

        // DELETE api/client/5
        [HttpDelete("{id}")]
        public ActionResult DeleteById(string id)
        {
            var clients = Repository.GetEntities(c => c.Id == id);
            if (clients.results?.Count() > 0)
            {
                foreach (var c in clients.results)
                {
                    c.IsActive = false;
                }
                Repository.SaveEntities(clients.results.ToArray());
                return Ok(clients);
            }
            else
            {
                return BadRequest();
            }
        }

    }
}