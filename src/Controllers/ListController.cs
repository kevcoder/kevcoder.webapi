using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using kevcoder.webapi.Models;
using kevcoder.webapi.Repository;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Configuration;
using Microsoft.Net.Http.Headers;
//using kevcoder.webapi.Models;

namespace kevcoder.webapi.Controllers
{
    [EnableCors]
    [Route("api/[controller]")]
    public class ListController : BaseController
    {
        public ListController(IConfiguration configuration, IRepository<Client> repository) : base(configuration, repository)
        {
        }

        [HttpGet]
        [Route("GetStates")]
        public ActionResult<State> GetStates()
        {
            var states = new List<State>();

            states.Add(new State()
            {
                Name = "Alabama",
                Abbreviation = "AL"
            });
            states.Add(new State()
            {
                Name = "Alaska",
                Abbreviation = "AK"
            });
            states.Add(new State()
            {
                Name = "American Samoa",
                Abbreviation = "AS"
            });
            states.Add(new State()
            {
                Name = "Arizona",
                Abbreviation = "AZ"
            });
            states.Add(new State()
            {
                Name = "Arkansas",
                Abbreviation = "AR"
            });
            states.Add(new State()
            {
                Name = "California",
                Abbreviation = "CA"
            });
            states.Add(new State()
            {
                Name = "Colorado",
                Abbreviation = "CO"
            });
            states.Add(new State()
            {
                Name = "Connecticut",
                Abbreviation = "CT"
            });
            states.Add(new State()
            {
                Name = "Delaware",
                Abbreviation = "DE"
            });
            states.Add(new State()
            {
                Name = "District Of Columbia",
                Abbreviation = "DC"
            });
            states.Add(new State()
            {
                Name = "Federated States Of Micronesia",
                Abbreviation = "FM"
            });
            states.Add(new State()
            {
                Name = "Florida",
                Abbreviation = "FL"
            });
            states.Add(new State()
            {
                Name = "Georgia",
                Abbreviation = "GA"
            });
            states.Add(new State()
            {
                Name = "Guam",
                Abbreviation = "GU"
            });
            states.Add(new State()
            {
                Name = "Hawaii",
                Abbreviation = "HI"
            });
            states.Add(new State()
            {
                Name = "Idaho",
                Abbreviation = "ID"
            });
            states.Add(new State()
            {
                Name = "Illinois",
                Abbreviation = "IL"
            });
            states.Add(new State()
            {
                Name = "Indiana",
                Abbreviation = "IN"
            });
            states.Add(new State()
            {
                Name = "Iowa",
                Abbreviation = "IA"
            });
            states.Add(new State()
            {
                Name = "Kansas",
                Abbreviation = "KS"
            });
            states.Add(new State()
            {
                Name = "Kentucky",
                Abbreviation = "KY"
            });
            states.Add(new State()
            {
                Name = "Louisiana",
                Abbreviation = "LA"
            });
            states.Add(new State()
            {
                Name = "Maine",
                Abbreviation = "ME"
            });
            states.Add(new State()
            {
                Name = "Marshall Islands",
                Abbreviation = "MH"
            });
            states.Add(new State()
            {
                Name = "Maryland",
                Abbreviation = "MD"
            });
            states.Add(new State()
            {
                Name = "Massachusetts",
                Abbreviation = "MA"
            });
            states.Add(new State()
            {
                Name = "Michigan",
                Abbreviation = "MI"
            });
            states.Add(new State()
            {
                Name = "Minnesota",
                Abbreviation = "MN"
            });
            states.Add(new State()
            {
                Name = "Mississippi",
                Abbreviation = "MS"
            });
            states.Add(new State()
            {
                Name = "Missouri",
                Abbreviation = "MO"
            });
            states.Add(new State()
            {
                Name = "Montana",
                Abbreviation = "MT"
            });
            states.Add(new State()
            {
                Name = "Nebraska",
                Abbreviation = "NE"
            });
            states.Add(new State()
            {
                Name = "Nevada",
                Abbreviation = "NV"
            });
            states.Add(new State()
            {
                Name = "New Hampshire",
                Abbreviation = "NH"
            });
            states.Add(new State()
            {
                Name = "New Jersey",
                Abbreviation = "NJ"
            });
            states.Add(new State()
            {
                Name = "New Mexico",
                Abbreviation = "NM"
            });
            states.Add(new State()
            {
                Name = "New York",
                Abbreviation = "NY"
            });
            states.Add(new State()
            {
                Name = "North Carolina",
                Abbreviation = "NC"
            });
            states.Add(new State()
            {
                Name = "North Dakota",
                Abbreviation = "ND"
            });
            states.Add(new State()
            {
                Name = "Northern Mariana Islands",
                Abbreviation = "MP"
            });
            states.Add(new State()
            {
                Name = "Ohio",
                Abbreviation = "OH"
            });
            states.Add(new State()
            {
                Name = "Oklahoma",
                Abbreviation = "OK"
            });
            states.Add(new State()
            {
                Name = "Oregon",
                Abbreviation = "OR"
            });
            states.Add(new State()
            {
                Name = "Palau",
                Abbreviation = "PW"
            });
            states.Add(new State()
            {
                Name = "Pennsylvania",
                Abbreviation = "PA"
            });
            states.Add(new State()
            {
                Name = "Puerto Rico",
                Abbreviation = "PR"
            });
            states.Add(new State()
            {
                Name = "Rhode Island",
                Abbreviation = "RI"
            });
            states.Add(new State()
            {
                Name = "South Carolina",
                Abbreviation = "SC"
            });
            states.Add(new State()
            {
                Name = "South Dakota",
                Abbreviation = "SD"
            });
            states.Add(new State()
            {
                Name = "Tennessee",
                Abbreviation = "TN"
            });
            states.Add(new State()
            {
                Name = "Texas",
                Abbreviation = "TX"
            });
            states.Add(new State()
            {
                Name = "Utah",
                Abbreviation = "UT"
            });
            states.Add(new State()
            {
                Name = "Vermont",
                Abbreviation = "VT"
            });
            states.Add(new State()
            {
                Name = "Virgin Islands",
                Abbreviation = "VI"
            });
            states.Add(new State()
            {
                Name = "Virginia",
                Abbreviation = "VA"
            });
            states.Add(new State()
            {
                Name = "Washington",
                Abbreviation = "WA"
            });
            states.Add(new State()
            {
                Name = "West Virginia",
                Abbreviation = "WV"
            });
            states.Add(new State()
            {
                Name = "Wisconsin",
                Abbreviation = "WI"
            });
            states.Add(new State()
            {
                Name = "Wyoming",
                Abbreviation = "WY"

            });


            return Ok(states);
        }
    }

}