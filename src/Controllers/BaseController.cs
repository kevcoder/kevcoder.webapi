using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using kevcoder.webapi.Models;
using kevcoder.webapi.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
//using kevcoder.webapi.Models;

namespace kevcoder.webapi.Controllers
{

    public class BaseController : ControllerBase
    {
        protected IConfiguration Configuration { get; set; }
        protected IRepository<Client> Repository { get; set; }
        public BaseController(IConfiguration configuration, IRepository<Client> repository)
        {
            this.Configuration = configuration;
            this.Repository = repository;
        }

    }
}