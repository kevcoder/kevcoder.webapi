using System;
namespace kevcoder.webapi.Models
{
    public class Invoice: Document
    {
        public string  Description { get; set; }
        public DateTime InvoiceDt { get; set; }
        public decimal AmountBilled { get; set; }
        public string InvoiceID { get; set; }
    }
}