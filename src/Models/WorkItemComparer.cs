using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace kevcoder.webapi.Models
{
    public class WorkItemComparer : EqualityComparer<WorkItem>
    {
        public override bool Equals([AllowNull] WorkItem x, [AllowNull] WorkItem y)
        {
            var xDt = (x?.CreationDt.HasValue == true) ? x.CreationDt.Value.Date : DateTime.Today;
            var yDt = (y?.CreationDt.HasValue == true) ? y.CreationDt.Value.Date : DateTime.Today;

            if (x == null && y == null)
                return true;
            else if (x == null || y == null)
                return false;
            else if (x.BillableTime.Hours.Equals(y.BillableTime.Hours)
            && x.BillableTime.Minutes.Equals(y.BillableTime.Minutes)
            && xDt.Equals(yDt))
                return true;
            else
                return false;
        }



        public override int GetHashCode([DisallowNull] WorkItem obj)
        {
            return (obj.BillableTime.GetHashCode() ^ obj.CreationDt.GetHashCode()).GetHashCode();
        }

    }
}