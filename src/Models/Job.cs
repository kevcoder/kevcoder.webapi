using System;
using System.Collections.Generic;

namespace kevcoder.webapi.Models
{
    public enum BillTypes
    {
        Hourly,
        Flat,
        Project,
        Other

    }
    public class Job 
    {
        public DateTime CreationDt { get; set; } = DateTime.Today;
        public string Name { get; set; }
        public string Description { get; set; }
        public IList<WorkItem> WorkItems { get; set; } = new List<WorkItem>();
        public decimal BillRate { get; set; }
        public BillTypes BillTypes { get; set; } = BillTypes.Hourly;

        //client information
        public DateTime? DueDt { get; set; }
        public DateTime? ClientApprovalDt { get; set; }
        public string ApprovedBy { get; set; }

        // job closing
        public DateTime? CompletionDt { get; set; }
        public DateTime? ClosedDt { get; set; }
        public decimal AmountCollected { get; set; }


    }
}