using System;
using src.Models;

namespace kevcoder.webapi.Models
{
    public class WorkItem 
    {
        public string Description { get; set; }
        public BillableTime BillableTime { get; set; } = new BillableTime(){Hours=0, Minutes = 0, MinHours = 0, MinMinutes = 0};
        public DateTime? CreationDt { get; set; }
    }
}