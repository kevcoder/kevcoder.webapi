using kevcoder.webapi.Models;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;

public class Document : IDocument
{

    [BsonId(IdGenerator = typeof(StringObjectIdGenerator))]
    public virtual string Id { get; set; }
}