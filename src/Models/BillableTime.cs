namespace src.Models
{
    public class BillableTime
    {
        public int Hours { get; set; }
        public int Minutes { get; set; }

        public int MinHours { get; set; }
        public int  MinMinutes { get; set; }
        
    }
}