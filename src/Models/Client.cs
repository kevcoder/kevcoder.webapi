using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;

namespace kevcoder.webapi.Models
{
    public class Client: Document
    {
        public string Name { get; set; }
        public string AddrLine1 { get; set; }
        public string AddrLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string ContactFirst { get; set; }
        public string ContactLastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public IList<Job> Jobs { get; set; } = new List<Job>();
        public bool IsActive { get; set; }

    }
}