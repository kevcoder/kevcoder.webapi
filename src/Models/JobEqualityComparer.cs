
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace kevcoder.webapi.Models
{
    public class JobEqualityComparer : IEqualityComparer<Job>
    {
        public bool Equals([AllowNull] Job x, [AllowNull] Job y)
        {
            if (x == null && y == null)
                return true;
            else if (x == null || y == null)
                return false;
            else if (x.Name.Equals(y.Name, StringComparison.InvariantCultureIgnoreCase)
            && x.WorkItems.Except(y.WorkItems).Count() == 0)
                return true;
            else
                return false;
        }

        public int GetHashCode([DisallowNull] Job obj)
        {
            return (obj.Name ?? "").GetHashCode(StringComparison.InvariantCultureIgnoreCase);
        }
    }
}