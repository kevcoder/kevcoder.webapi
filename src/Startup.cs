using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using kevcoder.webapi.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using kevcoder.webapi.Models;
using System.Text.Json.Serialization;
using System.Text.Json;
using Microsoft.Azure.Cosmos.Fluent;
using Microsoft.Azure.Cosmos;

namespace kevcoder.webapi
{
    public class Startup
    {
        private readonly string CORS_POLICY_NAME = "kevin";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "kevcoder.webapi", Version = "v1" });
                //should not be required in future versions: https://github.com/domaindrivendev/Swashbuckle.AspNetCore/issues/1269
            });

            services.AddCors(opts =>
            {
                opts.AddDefaultPolicy(bldr =>
               {
                   bldr.AllowAnyHeader().AllowAnyOrigin().AllowAnyMethod();
               });
            });

            services.AddControllers().AddJsonOptions(o =>
            {
                o.JsonSerializerOptions.AllowTrailingCommas = true;
                o.JsonSerializerOptions.IgnoreNullValues = false;
                o.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter(JsonNamingPolicy.CamelCase));

            });

            if (!Configuration.GetSection("RuntimeEnvironment").Value.Equals("azure",StringComparison.InvariantCultureIgnoreCase))
                services.AddSingleton<IRepository<Client>>(sp => new RavenDBClientRepository(Configuration.GetSection("urls:ravendb").Value));
            else
                services.AddSingleton<IRepository<Client>>(sp => new MnogoClientRepository(Configuration.GetSection("urls:mongoapi").Value));


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHttpsRedirection();
            }



            app.UseRouting();
            app.UseCors();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "kevcoder.webapi API V1");
            });
        }


    }
}
