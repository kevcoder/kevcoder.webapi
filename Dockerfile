FROM mcr.microsoft.com/dotnet/core/sdk:3.0 as build
WORKDIR /src
COPY ["kevcoder.webapi.csproj","./"]
# RUN dotnet restore "./kevcoder.webapi.csproj"
COPY . .
RUN dotnet build "kevcoder.webapi.csproj" -c Release -o /app

FROM build as publish
RUN dotnet publish "kevcoder.webapi.csproj" -c Release -o /app

FROM mcr.microsoft.com/dotnet/core/aspnet:3.0 as base
WORKDIR	/app
EXPOSE 80

FROM base as final
WORKDIR /app
COPY --from=publish /app .

ENV ASPNETCORE_ENVIRONMENT=Production
ENTRYPOINT [ "dotnet","kevcoder.webapi.dll" ]