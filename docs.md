# Docker 

## If you can use the docker-compose file

## other wise run these commands

1.  cd ./kevcoder.webapi

1.  docker run -d -p 38888:38888 -p 8081:8080 --network pinetwork --restart always --name nosqldb janniso/ravendb-arm --volume /home/pi/ravendb:/opt/RavenDB/Server/RavenData

1.  docker build -t kevcoder.webapi .

1.  docker run -d -p 5000:80 --network pinetwork --name webapi --restart  always kevcoder.webapi
