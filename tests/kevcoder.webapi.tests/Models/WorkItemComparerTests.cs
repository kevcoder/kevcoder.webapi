using Xunit;
using kevcoder.webapi.Models;
using src.Models;

namespace kevcoder.webapi.tests.Models
{
    public class WorkItemComparerTests
    {
        readonly WorkItemComparer _comparer;
        public WorkItemComparerTests()
        {
            _comparer = new WorkItemComparer();
        }
        
        [Fact]
        public void Equals_EmptyObjectVsNullObject_ReturnsFalse()
        {
            //Given            

            //When
            var actual = this._comparer.Equals(new WorkItem(), null);
            //Then
            Assert.False(actual, "comparing a default WorkItem to null");
        }

        [Fact]
        public void Equals_EmptyObjectVsEmptyObject_ReturnsTrue()
        {
            //Given
            var first = new WorkItem();
            var second = new WorkItem();
            //When
            var actual = _comparer.Equals(first, second);
            //Then
            Assert.True(actual, "comparing a default WorkItem to default WorkItem");
        }

        [Fact]
        public void Equals_DifferentDescription_ReturnsTrue()
        {
            //Given
            var first = new WorkItem(){Description = "first"};;
            var second = new WorkItem(){Description = "second"};

            //When
            var actual = _comparer.Equals(first, second);
            //Then
            Assert.True(actual, "the description is not included in the comparison"); 
        }

        [Fact]
        public void Equals_SameBillableTimeAndDifferentCreationDateTime_ReturnsTrue()
        {
            //Given
            var firstT = new BillableTime(){Hours=1, Minutes = 1};
            var secondT = new BillableTime(){Hours = 1, Minutes = 1};

            System.DateTime firstDt = new System.DateTime(2000,01,01,12,00,00);            
            System.DateTime secondDt = new System.DateTime(2000,01,01,11,00,00);

            var first = new WorkItem(){ BillableTime = firstT, CreationDt = firstDt};;
            var second = new WorkItem(){ BillableTime = secondT, CreationDt = secondDt};;

            //When
            var actual = _comparer.Equals(first, second);
            //Then
            Assert.True(actual, "the description is not included in the comparison"); 
        }
    }
}